#!/usr/bin/python
import os
from datetime import datetime, timedelta
from urllib.error import HTTPError
from urllib.request import urlopen

from bs4 import BeautifulSoup


def toogle_date_format(date):
    if isinstance(date, datetime):
        return datetime.strftime(date, '%y%m%d')
    elif isinstance(date, str):
        return datetime.strptime(date, '%y%m%d')
    else:
        raise('Unknown date format')


def get_days_before(date, limit=15):
    if date < datetime.now() - timedelta(limit):
        print(date, 'on if')
        return date
    else:
        print(date, 'on else')
        date = date - timedelta(1)
        return (get_days_before(date))


def get_nasa_apod_img_link(days_limit=5):
    browser = urlopen("http://apod.nasa.gov/apod/astropix.html")
    html = BeautifulSoup(browser.read(), 'html.parser')
    browser.close()


def download_NASA_APOD_wallpaper(save_path):
    browser = urlopen("http://apod.nasa.gov/apod/astropix.html")
    html = BeautifulSoup(browser.read(), 'html.parser')
    browser.close()
    try:
        href = html.find("img").parent.get("href")
        image_link = "http://apod.nasa.gov/apod/" + href
        print(image_link)
    except (HTTPError, AttributeError):
        yesterday = datetime.strftime(datetime.now() - timedelta(1), '%y%m%d')
        browser = urlopen("http://apod.nasa.gov/apod/ap%s.html" % (yesterday))
        html = BeautifulSoup(browser.read(), 'html.parser')
        browser.close()
        href = html.find("img").parent.get("href")
        image_link = "http://apod.nasa.gov/apod/" + \
            html.find("img").parent.get("href")
    save_path = os.path.expanduser(save_path)
    image_name = image_link.split("/")[-1]
    store_path = os.path.dirname(save_path) + "/" + image_name

    if os.path.isfile(store_path):
        return

    browser = urlopen(image_link)
    image_file = open(save_path, "wb")
    image_file.write(browser.read())
    image_file.close()
    browser.close()

    os.system("feh --bg-max %s" % save_path)

    # other wallpaper management tools
    # os.system("cp \"" + save_path + "\" \"" + store_path + "\"")
    # os.system("cp \"" + save_path + "\" ~/.xmonad/Wallpaper.jpg")
    # os.system("cp \"" + save_path + "\" ~/Pictures/Wallpaper.jpg")
    # os.system("gsettings set com.canonical.unity-greeter background %s" %
    #           save_path)
    # os.system("gsettings set org.gnome.desktop.background picture-uri \
    #           'file:///%s'" % save_path)


if __name__ == "__main__":
    download_NASA_APOD_wallpaper("~/Pictures/APOD/Wallpaper.jpg")
